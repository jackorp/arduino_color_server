#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif
#define PIN       2 // Data pin controlling LED strip
#define NUMPIXELS 120 // Number of total LEDs on a strip.
// esp-01 - web server
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ArduinoWebsockets.h>
// Over The Air updates for ease of use. Chip must have at least 50% of memory available for the firmware for successful update.
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#ifndef STASSID
#define STASSID "stassid"
#define STAPSK  "stapsk"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

// IP address config - must be valid and available in the current LAN
IPAddress ip(192, 168, 1, 1);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 255);

ESP8266WebServer server(80);

websockets::WebsocketsClient client;

// Enables printing to serial in case of debugging.
boolean debug = false;

/*
 * Debug endpoint in case we want to validate what query does the server receive.
 */
void handleRoot() {
	String message = "Debug site\n\n";
	message += "\nMethod: ";
	message += (server.method() == HTTP_GET) ? "GET" : "POST";
	message += "\nArguments: ";
	message += server.args();
	message += "\n";
	for (uint8_t i = 0; i < server.args(); i++) {
		message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
	}
	server.send(200, "text/plain", message);
}

void handleLED() {
	server.send(200, "text/plain", "Received");

	if (server.arg("method") == "multi")
		setMultiColor(server);

	if (server.arg("method") == "single")
		setSingleColor(server);

	if (server.arg("method") == "multi_sequence")
		setMultipleColorSequence(server);

	if (server.arg("method") == "single_sequence")
		setSingleColorSequence(server);
}


/*
 * HTTP request for the websocket to open/close connection to server location specified by the arguments 'ip' and 'port'
 */
void handleWebSocket() {
	String ip = server.arg("ip");
	String port = server.arg("port");
	String method = server.arg("method");
	String server_url = "ws://" + ip + ":" + port + "/";
	
	if (method == "open") {
		client.connect(server_url);
	} else if (method == "close") {
		client.close();
	}
	server.send(200, "text/plain", "url: '" + server_url + "' , method: " + method);
}

void onMessageCallback(websockets::WebsocketsMessage message) {
	pixels.clear();

	byte colors[3] = { 0 };
	int index_start = 0;
	int index_end = 0;
	int i;
	String data = message.data();

	for (i = 0; i <= NUMPIXELS; i++) {
		index_start = data.indexOf("[") + 1;
		index_end = data.indexOf("]");

		String color_str = data.substring(index_start, index_end);

		parseColors(color_str, colors);
		pixels.setPixelColor(i, pixels.Color(colors[0], colors[1], colors[2]));
		data = data.substring(index_end+1);

		index_start = 0;
		index_end = 0;
		/* client.send(color_str); Just for debug */
	}

	pixels.show();
}

void onEventsCallback(websockets::WebsocketsEvent event, String data) {
    if(event == websockets::WebsocketsEvent::ConnectionOpened) {
	    //onOpen
    } else if(event == websockets::WebsocketsEvent::ConnectionClosed) {
	    //onClose
    } else if(event == websockets::WebsocketsEvent::GotPing) {
        client.send("Got a Ping!");
    } else if(event == websockets::WebsocketsEvent::GotPong) {
        client.send("Got a Pong!");
    }
}

/*
 * Set multiple colors across the LED strip.
 */
void setMultiColor(ESP8266WebServer& server) {
	pixels.clear();
	int delayVal = server.arg("delay").toInt();
	// Server args will be consumed here, 0th index is method to use
	// so color definition starts with 1st argument.
	int i;
	for (i = 0; i < NUMPIXELS; i++) {
		byte color[3] = { 0 };
		parseColors(server.arg(i+2), color); // offset to where color definition start
		pixels.setPixelColor(i, pixels.Color(color[0], color[1], color[2]));
		delay(delayVal);
		pixels.show();
	}
}

/*
 * Set a single color across the LED strip
 */
void setSingleColor(ESP8266WebServer& server) {
	pixels.clear();
	byte color[3] = { 0 };

	parseColors(server.arg(2), color);

	int i;
	for (i = 0; i < NUMPIXELS; i++) {
		pixels.setPixelColor(i, pixels.Color(color[0], color[1], color[2]));
	}
	pixels.show();
}

/*
 * We have multiple colors we want to set in sequence.
 * Should be in multiples of NUMPIXELS
 * Single sequence means NUMPIXELS of colors in query arguments.
 * Delay after each color can be specified in the query arguments.
 */
void setMultipleColorSequence(ESP8266WebServer& server) {
	pixels.clear();

	int delayVal = server.arg("delay").toInt();

	int i;

	// first server arg is method and second one is delayVal, offset it by that amount.
	for (i=2; i < server.args(); i++) {
		byte color[3] = { 0 };
		parseColors(server.arg(i), color);
		int j = 0;
		for (j; j < NUMPIXELS; j++) {
			pixels.setPixelColor(j, pixels.Color(color[0], color[1], color[2]));
			pixels.show();
			delay(delayVal);
		}
	}
}

/*
 * We have multiple colors we want to show in a sequence.
 * Sets whole strip to a color supplied in query arguments.
 * Delay after each color can be specified in the query arguments.
 */
void setSingleColorSequence(ESP8266WebServer& server) {
	pixels.clear();
	
	int delayVal = server.arg("delay").toInt();

	int i;

	for (i=2; i < server.args(); i++) {
		int j = 0;
		for (j; j < NUMPIXELS; j++) {
			byte color[3] = { 0 };
			parseColors(server.arg(i), color);
			pixels.setPixelColor(j, pixels.Color(color[0], color[1], color[2]));
		}
		pixels.show();
		delay(delayVal);
	}
}

void parseColors(String str, byte colors[]) {
	if (debug) Serial.println("************************");
	byte red = 0;
	byte green = 0;
	byte blue = 0;
	byte redIndex = str.indexOf(",");
	byte greenIndex = str.indexOf(",", redIndex + 1);
	red = str.substring(0, redIndex).toInt();
	green = str.substring(redIndex + 1, greenIndex).toInt();
	blue = str.substring(greenIndex + 1).toInt();
	colors[0] = red;
	colors[1] = green;
	colors[2] = blue;
	if (debug) Serial.println(red);
	if (debug) Serial.println(green);
	if (debug) Serial.println(blue);
}

/***********************************************\
 *						|
 *						|
 *		     SETUP			|
 *						|
 *						|
 ***********************************************/
void setup() {
	// Some chip has some limitations, including this bit of code won't do any harm
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
	clock_prescale_set(clock_div_1);
#endif

	pinMode(PIN, OUTPUT);
	// komunikace po sériové lince rychlostí 115200 baud
	if (debug) Serial.begin(115200);
	if (debug) Serial.println("Booting");
	// zapnutí komunikace s Ethernet Shieldem
	WiFi.config(ip, gateway, subnet);
	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);
	if (debug) Serial.println("");

	while (WiFi.waitForConnectResult() != WL_CONNECTED) {
		delay(500);
		if (debug) Serial.print(".");
	}

	// Port defaults to 8266
	ArduinoOTA.setPort(8266);

	// Hostname defaults to esp8266-[ChipID]
	ArduinoOTA.setHostname("esp8266");

	// No authentication by default
	ArduinoOTA.setPassword("mypass");

	// Password can be set with it's md5 value as well
	//ArduinoOTA.setPasswordHash("myhash");

	ArduinoOTA.onStart([]() {
			String type;
			if (ArduinoOTA.getCommand() == U_FLASH) {
			type = "sketch";
			} else { // U_FS
			type = "filesystem";
			}

			// NOTE: if updating FS this would be the place to unmount FS using FS.end()
			if (debug) Serial.println("Start updating " + type);
			});
	ArduinoOTA.onEnd([]() {
			if (debug) Serial.println("\nEnd");
			});
	ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
			if (debug) Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
			});
	ArduinoOTA.onError([](ota_error_t error) {
			if (debug) Serial.printf("Error[%u]: ", error);
			if (error == OTA_AUTH_ERROR) {
			if (debug) Serial.println("Auth Failed");
			} else if (error == OTA_BEGIN_ERROR) {
			if (debug) Serial.println("Begin Failed");
			} else if (error == OTA_CONNECT_ERROR) {
			if (debug) Serial.println("Connect Failed");
			} else if (error == OTA_RECEIVE_ERROR) {
			if (debug) Serial.println("Receive Failed");
			} else if (error == OTA_END_ERROR) {
			if (debug) Serial.println("End Failed");
			}
			});


	if (MDNS.begin("esp8266")) {
		if (debug) Serial.println("MDNS responder started");
	}

	/* digitalWrite(2, LOW); */

	server.on("/", handleRoot);

	server.on("/my_endpoint", handleLED);

	server.on("/websocket", handleWebSocket);

	// Setup Callbacks
	client.onMessage(onMessageCallback);
	client.onEvent(onEventsCallback);

	server.begin();

	pixels.begin();

	ArduinoOTA.begin();

	if (debug) Serial.println("HTTP server started");
}

void loop() {
	server.handleClient();
	MDNS.update();

	ArduinoOTA.handle();

	client.poll();
}
