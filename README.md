# ArduinoColorServer

A sinatra based web server sending LED colors to another server.
Made originally for the ESP8266 on the esp-01.

## Installation

You must have all the requirements from gemspec installed first.

## Usage

Run `RUBYOPT=-Ilib ./bin/arduino_color_server` to start the server. Feel free to tweak Sinatra's options to suit your local environment as long as it stays on that environment.

## Development

Be sure that all changes are well integrated and actually do what they are supposed to as there are currently no proper tests.
If you're modifying a view make sure that design looks somewhat sane.

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/jackorp/arduino_color_server.


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
