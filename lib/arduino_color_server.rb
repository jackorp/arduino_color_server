require 'arduino_color_server/color_sequence'
require 'arduino_color_server/breathing'
require 'arduino_color_server/color_gradient'
require 'arduino_color_server/error'
require 'arduino_color_server/sendable'
require 'arduino_color_server/random'
require 'arduino_color_server/rainbow'
require 'arduino_color_server/color'
require 'arduino_color_server/cava_wrapper'
require 'arduino_color_server/cava_websocket'
require 'arduino_color_server/normalized_bar'
require 'arduino_color_server/version'

module ArduinoColorServer
  class Error < StandardError; end

  LED_COUNT = 120
end
