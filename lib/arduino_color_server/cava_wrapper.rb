require 'ruby-fifo'
require 'timeout'
require 'open3'
require 'singleton'
require 'fileutils'

module ArduinoColorServer

  class CavaWrapper
    # Exception class used when the Cava process is in unexpected state.
    class ProcessError < StandardError; end
  end

  class CavaWrapper
    include Singleton

    def initialize
      # Let's make sure the file is empty. Cava creates it for us.
      FileUtils.rm('/tmp/cava.fifo', force: true)
      @running = false

      @bars_count = 30
      # output_bit_format = '16bit'
      @byte_type = 'S' # 16 bit unsigned (uint16_t) as per cava config
      @byte_size = 2
      @byte_norm = 65535

      @fifo_stream = Fifo.new('/tmp/cava.fifo', :r, :nowait)
    end

    # Returns array of bars from cava.
    # Total number of bars is defined by +@bars_count+ in initialization
    # and in the cava configuration
    def chunk
      chunk_size = @byte_size * @bars_count
      format = @byte_type * @bars_count

      raw_data = @fifo_stream.read(chunk_size)
      
      raw_data
        .unpack(format)
        .map { |num| (num.to_f/@byte_norm).round(5) }
    end

    # Average sound intensity collected in specific timeframe
    def average_of(seconds)
      # 60 frames in a second as stated in cava config
      bars = Array.new((seconds * 60).round) { self.chunk }

      average_bars = bars.collect { |arr| arr.sum / arr.count }
      average_bars.sum / average_bars.count
    end

    # initialize the Cava process.
    # Raises +ArduinoColorServer::CavaWrapper::ProcessError+ in case there already
    # is a running process.
    def start
      raise ArduinoColorServer::CavaWrapper::ProcessError, "Cannot start cava process, is it running already? Cava PID: #{@cava_pid}, running: #{@running}" if @running

      cavain, cavaout, @cava_wait_thr = Open3.popen2("cava -p #{File.join(__dir__, '../../', 'cava_config.conf')}")

      @running = true

      # We don't care about input or output
      cavain.close
      cavaout.close

      @cava_pid = @cava_wait_thr.pid
    end

    # Terminate Cava process.
    # Raises +ArduinoColorServer::CavaWrapper::ProcessError+ in case the process is not running.
    def terminate
      raise ArduinoColorServer::CavaWrapper::ProcessError, "Cannot terminate cava, is it running? Cava PID: #{@cava_pid}, running: #{@running}" unless @running

      Process.kill("SIGINT", @cava_pid)

      @running = false
    end
  end
end
