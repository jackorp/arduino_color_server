require 'arduino_color_server/color_gradient'

module ArduinoColorServer
  # Bar remapping sound intensity to how many LEDs should be on.
  class NormalizedBar
    include ArduinoColorServer::ColorGradient

    attr_reader :led_count

    # +total_led_count+ - total number of LEDs.
    # +symmetry+ - if the bar should be symmetrical
    # +delay+ - unused so far.
    def initialize(total_led_count, symmetry: true, delay: 0)
      @symmetry = symmetry
      self.led_count = total_led_count
      @delay = delay
    end

    # Remaps the +sound_intensity+ to how many LEDs will be turned on.
    # TODO: it currently does not work with +symmetry: false+
    def sound_intensity_bar(colors, sound_intensity)
      gradient = multiple_color_gradient(colors, @led_count)
      on_led_count = remap_value(sound_intensity, 0.0, 1.0, 0, @led_count)
      symmetry_bar(gradient.first(on_led_count))
        .map { |color| "[#{color}]" }
        .join('')
    end

    private

    def led_count=(led_count)
      @led_count = @symmetry ? led_count / 2 : led_count
    end

    def symmetry_bar(arr)
      colors = arr.reverse + arr
      extra_leds_count = (ArduinoColorServer::LED_COUNT - colors.count) / 2
      black_leds = Array.new(extra_leds_count) { ArduinoColorServer::Color.new(0, 0, 0) }
      colors
        .append(black_leds)
        .prepend(black_leds)
        .flatten
    end

    # Rule of three to get the value in the +high+ range
    def remap_value(value, low1, high1, low2, high2)
      (low2 + (value.to_f - low1) * (high2 - low2) / (high1 - low1)).round
    end
  end
end
