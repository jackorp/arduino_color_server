require 'arduino_color_server/color_sequence'
require 'arduino_color_server/color_gradient'

module ArduinoColorServer
  # Manages creating breathing sequence from +color_array+.
  # NOTE: quite unstable, and highly experimental
  class Breathing < ColorSequence
    include ArduinoColorServer::ColorGradient

    # +color_array+ - array of raw number colors
    # +steps+ - total steps between gradients
    def initialize(color_array = [[255,0,0], [0,0,255]], steps = 100, delay: 100, slowing_factor: 2, ip: '')
      super delay, 'single_sequence', ip: ip

      @steps = steps
      colors = color_array.map { |color| Color.new(*color) }
      @colors = breathe(colors, slowing_factor)
      puts @colors
    end

    def breathe(colors, slowing_factor)
      gradient(colors).map.with_index do |color, i|
        Color.new(
          breathe_step(color.r, i, slowing_factor),
          breathe_step(color.g, i, slowing_factor),
          breathe_step(color.b, i, slowing_factor),
        )
      end
    end

    def breathe_step(color_component, step_number, slowing_factor)
      (Math.sin(step_number * Math::PI / slowing_factor).abs*color_component).round
    end

    def gradient(colors)
      colors.map.with_index do |color, i|
        end_color = @colors[i+1]
        end_color ||= Color.new(0,0,0)
        gradient_from(color, end_color, @steps)
      end.flatten
    end
  end
end
