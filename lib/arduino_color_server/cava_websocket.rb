require 'nio/websocket'
NIO::WebSocket.logger.level = Logger::DEBUG
module ArduinoColorServer
  # +ArduinoColorServer::CavaWebSocket+ handles websocket logic and sending data from cava wrapper.
  class CavaWebSocket
    attr_reader :poll_time, :server, :open, :ip, :port

    # +cava+ - +ArduinoColorServer::CavaWrapper+ instance
    # +color_bar+ - +ArduinoColorServer::NormalizedBar+ instance
    # +ip+ - server address that the server is running on. Should be reachable at least in LAN, e.g. 192.168.1.80
    # +port+ - port number websocket server binds to
    # +poll_time+ - timespan for collecting sound data from cava.
    def initialize(cava, color_bar, ip, port, poll_time: 0.08)
      @cava = cava
      @color_bar = color_bar

      @ip = ip
      @port = port

      @open = false

      @running = false

      # rainbow
      @bar_colors = [ ArduinoColorServer::Color.new(148, 0, 211), ArduinoColorServer::Color.new(75, 0, 130), ArduinoColorServer::Color.new(0, 0, 255), ArduinoColorServer::Color.new(0, 255, 0), ArduinoColorServer::Color.new(255, 255, 0), ArduinoColorServer::Color.new(255, 127, 0), ArduinoColorServer::Color.new(255, 0, 0) ]

      @poll_time = poll_time

      @thr = nil
    end

    # Method for starting +NIO::WebSocket+ server listening on +@port+
    # NOTE: this class does care if you try to launch 2 servers on same port, so it should be checked from outside.
    def start
      @server = NIO::WebSocket.listen port: @port do |driver|
        @running = true
        callbacks.call(driver)
      end
    end

    # Is the server currently running?
    def running?
      @running
    end

    private

    def bar
      @color_bar.sound_intensity_bar(@bar_colors, @cava.average_of(@poll_time))
    end

    def send_bar_data(driver)
      Thread.new do
        loop do
          status = driver.text(bar)
          unless status
            puts 'something is wrong with websocket' \
              'cannot send data'
          end
        end
      end
    end

    # cleanup after closing a server
    def cleanup
      if @open
        Thread.kill(@thr)
        @cava.terminate
        @open = false
      end
    end

    def callbacks
      ->(driver) do
        driver.on :open do |event|
          puts 'websocket opened'
          @open = true
          @cava.start
          @thr = send_bar_data(driver)
        end

        driver.on :message do |event|
          puts "you got mail! #{event.data}"
        end

        driver.on :close do |event|
          cleanup
          puts 'websocket closed'
        end

        driver.on :io_error do |event|
          cleanup
          puts 'websocket IO error'
        end
      end
    end
  end
end
