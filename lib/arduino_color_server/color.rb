module ArduinoColorServer
  Color = Struct.new(:r, :g, :b) do
    alias :red   :r
    alias :green :g
    alias :blue  :b

    def color
      self.to_a
    end

    def to_s
      "#{r},#{g},#{b}"
    end
  end
end
