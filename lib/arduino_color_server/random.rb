require 'arduino_color_server/color_sequence'

module ArduinoColorServer
  # Random sequence of colors.
  class Random < ColorSequence
    def initialize(delay: 0, ip: '')
      super delay, ip: ip

      @colors = [[255,0,0], [161,0,0],[162,82,3], [161,161,0], [101,130,0],[65,102,0],[7,136,70], [0,130,130],[0,65,130],[0,33,203],[68,10,127],[106,0,106],[153,0,77]].map { |arr| Color.new(*arr) }
    end

    def execute
      swithcer = -> { @colors.shuffle.each { |color| send_request(color, delay: @delay, request_method: 'single'); sleep(0.300) } }
      (50).times { swithcer.call }
    end
  end
end
