module ArduinoColorServer
  # +ArduinoColorServer::ColorGradient+ is a mixin module that provides methods for
  # creating gradients between colors.
  module ColorGradient

    #
    # :args: end_color, resolution
    # Gradient from +@color+ (instance variable) to +end_color+.
    # +resolution+ defines
    def gradient_to(end_color, step_count)
      Array.new(step_count) { |i| step @color, end_color, i, step_count }
    end

    #
    # :args: start_color, end_color. resolution
    # Gradient from +start_color+ to +end_color+.
    # +resolution+ defines how many colors will be in between.
    def gradient_from(start_color, end_color, step_count)
      Array.new(step_count) { |i| step start_color, end_color, i, step_count }
    end

    #
    # :args: colors, resolution
    # Gradient between +colors+.
    # +resolution+ defines total output of colors.
    # Number of gradient colors between each color in array is defined as follows:
    # +resolution/colors.count+
    def multiple_color_gradient(color_array, total_steps)
      color_gradient = color_array.map.with_index do |color, i|
        end_color = color_array[i+1]
        end_color ||= Color.new(0,0,0)
        gradient_from(color, end_color, total_steps/color_array.count)
      end.flatten

      # Not everything is nicely dividable... Let's fill in the gap
      if total_steps > color_gradient.count
        extra_leds = total_steps - color_gradient.count
        extra_leds.times { color_gradient << color_gradient.last }
      end

      color_gradient
    end

    #
    # :call-seq:
    #   gradient.step(start_color, end_color, step_number, resolution) -> ArduinoColorServer::Color
    #
    # Create a new +ArduinoColorServer::Color+ representing a single part in full gradient.
    # +step_number+ must be less than or equal to +resolution+
    def step(start, ending, step_number, total_steps)
      raise 'Step number cannot be higher than resolution' if step_number > total_steps

      r = step_color_part start.r, ending.r, step_number, total_steps
      g = step_color_part start.g, ending.g, step_number, total_steps
      b = step_color_part start.b, ending.b, step_number, total_steps
      Color.new r, g, b
    end

    #
    # Return higher or lower color component value depending on values of start and end color.
    def step_color_part(start, ending, step_number, total_steps)
      unless start == ending
        # at least one part must be float so the math is solved correctly.
        (((ending-start)/total_steps.to_f) * step_number + start).round
      else
        start
      end
    end
  end
end
