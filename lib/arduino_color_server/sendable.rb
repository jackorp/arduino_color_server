require 'httparty'
require 'arduino_color_server/error'

module ArduinoColorServer
  # Mixin module managing sending the color requests.
  module Sendable

    VALID_METHODS = ['single', 'single_sequence', 'multi', 'multi_sequence'].freeze

    private

    # +color_array+ - array of +ArduinoColorServer::Color+ objects
    # Sends the array with +delay+ and +method+ to +@ip+
    def send_request(color_array, delay: 0, request_method: 'single')
      check_method request_method
      validate_colors_count(color_array, request_method)
      colors = compose_color_query color_array if color_array.kind_of? Array
      colors ||= color_array
      HTTParty.get(@ip, query: { :method => request_method, :delay => delay, :color => colors})
    end

    private

    def compose_color_query(color_array)
      color_array.map(&:to_s)
    end

    def check_method(method_name)
      raise ArduinoColorServer::Error, "Invalid color method name: #{method_name}" unless VALID_METHODS.any? method_name
    end

    def validate_colors_count(colors, request_method)
      if colors.is_a? Array
        raise ArduinoColorServer::Error, "You cannot specify multiple colors with 'single' request method!" if colors.count > 1 and request_method.eql? 'single'
      end

      if colors.is_a? String
        raise ArduinoColorServer::Error, "You cannot specify multiple colors with a string. Use array instead" if colors.count(',') > 2
      end
    end
  end
end
