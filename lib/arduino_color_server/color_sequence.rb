require 'arduino_color_server/sendable'

module ArduinoColorServer
  # Main class for most other color sequence classes, defines basics like sending requests and some variable initializations.
  class ColorSequence
    include ArduinoColorServer::Sendable

    attr_writer :ip

    def initialize(delay = 0, request_method = 'single', led_count = 1, ip: 'http://192.168.1.1:80/')
      @ip = ip
      @request_method = request_method
      @delay = delay
      @led_count = 1
      @colors = []
    end

    def execute
      send_request(@colors, delay: @delay, request_method: @request_method)
    end
  end
end
