require 'arduino_color_server/color_sequence'
require 'arduino_color_server/color_gradient'

module ArduinoColorServer
  # Creates rainbow either laid out as a gradient on the LED strip or sends the colors 1 by 1 as a sequence.
  # TODO: If there will be +gradient+ called on the object it cannot go back to the original colors...
  class Rainbow < ColorSequence
    include ArduinoColorServer::ColorGradient

    def initialize(method = nil, delay: nil, ip: '')
      delay ||= 200
      super delay, ip: ip
      # Rainbow:  purple    , indigo    ,  blue    ,  green   ,  yellow    ,  orange    ,  red
      @colors = [ Color.new(148, 0, 211), Color.new(75, 0, 130), Color.new(0, 0, 255), Color.new(0, 255, 0), Color.new(255, 255, 0), Color.new(255, 127, 0), Color.new(255, 0, 0) ]
      @request_method = 'single_sequence'
    end

    def execute
      send_request(@colors, delay: @delay, request_method: @request_method)
    end

    def gradient
      @colors = multiple_color_gradient(@colors, ArduinoColorServer::LED_COUNT)
      @request_method = 'multi'
      self
    end
  end
end
