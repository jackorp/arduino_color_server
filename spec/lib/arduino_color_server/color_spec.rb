require 'arduino_color_server/color'

RSpec.describe ArduinoColorServer::Color do
  let(:color) { ArduinoColorServer::Color.new(0, 1, 2) }

  describe '#red' do
    it 'returns the red component'do
      expect(color.red).to eq 0
    end
  end

  describe '#green' do
    it 'returns the green component' do
      expect(color.green).to eq 1
    end
  end

  describe '#blue' do
    it 'returns the blue component' do
      expect(color.blue).to eq 2
    end
  end

  describe '#color' do
    it 'returns the color as an array' do
      expect(color.color).to eq [0, 1, 2]
    end
  end

  describe '#to_s' do
    it 'returns a comma seperated color string' do
      expect(color.to_s).to eq "0,1,2"
    end
  end
end
