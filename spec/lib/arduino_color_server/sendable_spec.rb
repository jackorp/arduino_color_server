require 'arduino_color_server/sendable'

RSpec.describe ArduinoColorServer::Sendable do
  let(:ip) { 'fake.ip' }
  let(:color) { '1,2,3' }
  let(:default_req_method) { 'single' }

  class Foo
    include ArduinoColorServer::Sendable

    attr_reader :ip

    def initialize(ip)
      @ip = ip
    end
  end

  let(:sendable) { Foo.new(ip) }

  describe '#send_request' do
    context 'with valid request_method' do
      %w(single multi single_sequence multi_sequence).each do |meth|
        context "with #{meth}" do
          it 'sends HTTP request to specified IP' do
            expect(HTTParty).to receive(:get)
              .with(ip, query: { method: meth, delay: 0, color: color })

            sendable.send(:send_request, color, delay: 0, request_method: meth)
          end
        end
      end
    end

    context 'when string is passed' do
      context 'with invalid request method' do
        it 'returns exception' do
          expect { sendable.send(:send_request, color, delay: 0, request_method: 'invalid_foobar') }.to raise_error(ArduinoColorServer::Error)
        end
      end

      context 'with a single color as a string' do
        it 'uses that string as a part of query' do
          expect(HTTParty).to receive(:get).with(ip, query: { method: default_req_method, delay: 0, color: color })

          sendable.send(:send_request, color)
        end
      end

      context 'with multiple colors as a string' do
        let(:multi_color_string) { "#{color} #{color}" }

        before do
          allow(HTTParty).to receive(:get)
        end

        it 'raises an exception' do
          expect { sendable.send(:send_request, multi_color_string, delay: 0, request_method: default_req_method) }.to raise_error(ArduinoColorServer::Error)
        end
      end
    end

    context 'when array of color objects is passed' do
      let(:single_color_array) { [ ArduinoColorServer::Color.new(1, 2, 3) ] }
      let(:multi_color_array) do
        Array.new(3) do |num|
          num += 1
          ArduinoColorServer::Color.new(1 * num, 2 * num, 3 * num)
        end
      end

      context 'with single request method' do
        let(:request_method) { 'single' }

        context 'with single color' do
          it 'sends the formatted RGB colors' do
            expect(HTTParty).to receive(:get).with(ip, query: { method: request_method, delay: 0, color: single_color_array.map(&:to_s) })

            sendable.send(:send_request, single_color_array, delay: 0, request_method: request_method)
          end
        end

        context 'with multiple colors' do
          before do
            allow(HTTParty).to receive(:get)
          end

          it 'raises an exception' do
            expect { sendable.send(:send_request, multi_color_array, delay: 0, request_method: request_method) }.to raise_error(ArduinoColorServer::Error)
          end
        end
      end

      context 'with multi request method' do
        let(:request_method) { 'multi' }

        context 'with single color' do
          before do
            allow(HTTParty).to receive(:get)
          end

          it 'raises an exception' do
            expect(HTTParty).to receive(:get).with(ip, query: { method: request_method, delay: 0, color: multi_color_array.map(&:to_s) })

            sendable.send(:send_request, multi_color_array, delay: 0, request_method: request_method)
          end
        end

        context 'with multiple colors' do
          it 'sends the formatted RGB colors' do
            expect(HTTParty).to receive(:get).with(ip, query: { method: request_method, delay: 0, color: multi_color_array.map(&:to_s) })

            sendable.send(:send_request, multi_color_array, delay: 0, request_method: request_method)
          end
        end
      end

      context 'with single_sequence request method' do
        let(:request_method) { 'single_sequence' }

        context 'with single color array' do
          it 'sends the formatted RGB colors' do
            expect(HTTParty).to receive(:get).with(ip, query: { method: request_method, delay: 0, color: multi_color_array.map(&:to_s) })

            sendable.send(:send_request, multi_color_array, delay: 0, request_method: request_method)
          end
        end

        context 'with multiple colors' do
          it 'sends the formatted RGB colors' do
            expect(HTTParty).to receive(:get).with(ip, query: { method: request_method, delay: 0, color: multi_color_array.map(&:to_s) })

            sendable.send(:send_request, multi_color_array, delay: 0, request_method: request_method)
          end
        end
      end

      context 'with multi_sequence request method' do
        let(:request_method) { 'multi_sequence' }

        context 'with single color array' do
          it 'sends the formatted RGB colors' do
            expect(HTTParty).to receive(:get).with(ip, query: { method: request_method, delay: 0, color: multi_color_array.map(&:to_s) })

            sendable.send(:send_request, multi_color_array, delay: 0, request_method: request_method)
          end
        end

        context 'with multiple colors' do
          it 'sends the formatted RGB colors' do
            expect(HTTParty).to receive(:get).with(ip, query: { method: request_method, delay: 0, color: multi_color_array.map(&:to_s) })

            sendable.send(:send_request, multi_color_array, delay: 0, request_method: request_method)
          end
        end
      end
    end
  end
end
