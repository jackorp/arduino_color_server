require_relative 'lib/arduino_color_server/version'

Gem::Specification.new do |spec|
  spec.name          = "arduino_color_server"
  spec.version       = ArduinoColorServer::VERSION
  spec.authors       = ["Jaroslav Prokop"]
  spec.email         = ["jar.prokop@volny.cz"]

  spec.summary       = %q{Web server for sending color codes to arduino over HTTP.}
  spec.description   = %q{Web server including frontend to send color codes over HTTP to arduino to light up multi-color LED.}
  spec.homepage      = "TODO: Put your gem's website or public repo URL here."
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://www.gitlab/jackorp/arduino_color_server"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end

  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency 'sinatra'
  spec.add_runtime_dependency 'haml'
  spec.add_runtime_dependency 'httparty'
  spec.add_runtime_dependency 'ruby-fifo'
  spec.add_runtime_dependency 'nio4r-websocket'
  spec.add_development_dependency 'rake', '>= 12'
  spec.add_development_dependency 'rspec', '>= 3'
  spec.add_development_dependency 'guard-livereload', '>= 2.5.2'
end
