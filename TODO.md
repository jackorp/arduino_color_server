# Roadmap
* This should have tests.
* There are some TODOs in the library that need doing.
* WebSocket is kind of finnicky in its working. Better testing should make some problems easier to pinpoint.
* Have .gitlab\_ci.yml where tests are executed and documentation is generated as well as saved as artifact to gitlab pages.
